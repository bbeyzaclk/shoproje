package Servlet;
import java.io.IOException;

  
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;  
import javax.servlet.annotation.WebServlet;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;

import Customer.Customer;  
import Customerdao.Customerdao;
@WebServlet("/servlet")  
public class Save extends HttpServlet {  

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)   
         throws ServletException, IOException {  
        response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
         
        String CustID=request.getParameter("custID");  
        String Name=request.getParameter("name");  
        String Surname=request.getParameter("surname"); 
        String Store=request.getParameter("store"); 
        String Number1=request.getParameter("number1"); 
        String Number2=request.getParameter("number2"); 
        String Number3=request.getParameter("number3"); 
 
     
          
        Customer e = new Customer();
        e.setCustID(Integer.parseInt(CustID)); 
        e.setName(Name);  
        e.setSurname(Surname); 
        e.setStore(Store); 
        e.setNumber1(Number1); 
        e.setNumber2(Number2);  
        e.setNumber3(Number3);  
       
          
          
          
        int status = Customerdao.saveCustomer(e);  
    	try {
			if(status>0){
		        Customerdao.savePhone(e);

			
	            out.print("<p>Record saved successfully!</p>");  
	            request.getRequestDispatcher("index.html").include(request, response);  
	        }else{  
	            out.println("Sorry! unable to save record");  
	        }  
		} catch (Exception ex) {
			 
			ex.printStackTrace();
		}  
         
          
        out.close();  
    }  
}