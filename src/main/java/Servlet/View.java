package Servlet;

import java.io.IOException;  
import java.io.PrintWriter;  
import java.util.List;  
  
import javax.servlet.ServletException;  
import javax.servlet.annotation.WebServlet;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;

import Customerdao.Customerdao;
import Customer.Customer;  
@WebServlet("/ViewServlet")  
public class View extends HttpServlet {  
    
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)   
               throws ServletException, IOException {  
        response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        out.println("<a href='index.html'>Add New Customer</a>");  
        out.println("<h1>Customer List</h1>");  
          
        Customerdao customerdao = new Customerdao();
        List<Customer> list=customerdao.getAllCustomer();  
          
        out.print("<table border='1' width='100%'");  
        out.print("<tr><th>Name</th><th>Surname</th><th>CustID</th><th>Number</th><th>Store</th></tr>");  
        for(Customer e:list){  
         out.print("<tr><td>"+e.getName()+"</td><td>"+e.getSurname()+"</td><td>"+e.getCustID()+"</td>"
          		+ "<td>"+e.getNumber1()+"</td> <td>"+e.getStore()+"</td></tr>");  
        }  
        out.print("</table>");  
          
        out.close();  
    }  
} 