package Customerdao;

import java.util.*;

import Customer.Customer;

import java.sql.*;  
  
public class Customerdao {  
  
	  public static Connection getConnection(){  
	        Connection con=null;  
	        try{  
	            Class.forName("com.mysql.jdbc.Driver");  
	            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/Deneme","root","password");  
	        }catch(Exception e)
	        
	        {System.out.println(e);
	        }  
	        return con;  
	    }  
	  
	      
    public static int saveCustomer(Customer e){  
        int status=0;  
        try{  
            Connection con=Customerdao.getConnection();  
            PreparedStatement ps=con.prepareStatement(  
                         "INSERT INTO Customers(custID,name,surname,storeName) values (?,?,?,?)");  

            ps.setInt(1,e.getCustID());  
            ps.setString(2,e.getName());  
            ps.setString(3,e.getSurname());  
            ps.setString(4,e.getStore());  
            status=ps.executeUpdate();  
              
            con.close();  
        }catch(Exception ex){ex.printStackTrace();}       
        return status;  
    }  
    
    public static int savePhone(Customer e){  

        int status=0;  
        try{  
            Connection con=Customerdao.getConnection();  
            PreparedStatement ps=con.prepareStatement(  
                         "INSERT INTO Phone(custID,number1,number2,number3) values (?,?,?,?)");  
        
            ps.setInt(1,e.getCustID());  
            ps.setString(2,e.getNumber1());  
            ps.setString(3,e.getNumber2()); 
            ps.setString(4,e.getNumber3()); 
            status=ps.executeUpdate();  
              
            con.close();  
        }catch(Exception ex){ex.printStackTrace();}  
          
        return status;  
    } 
    
    public static int update(Customer e){  
        int status=0;  
        try{  
            Connection con=Customerdao.getConnection();  
            PreparedStatement ps=con.prepareStatement(  
                         "UPDATE Customers set name=?,surname=?,storeName=? where custID=?");  
            ps.setString(1,e.getName());  
            ps.setString(2,e.getSurname()); 
            ps.setString(3,e.getStore());  
            ps.setInt(4,e.getCustID());  
              
            status=ps.executeUpdate();  
              
            con.close();  
        }catch(Exception ex){ex.printStackTrace();}  
          
        return status;  
    }  
    public static int delete(int custID){  
        int status=0;  
        try{  
            Connection con=Customerdao.getConnection();  
            PreparedStatement ps=con.prepareStatement("Delete From Customers where custID=?");  
            ps.setInt(1,custID);  
            status=ps.executeUpdate();  
              
            con.close();  
        }catch(Exception e){e.printStackTrace();}  
          
        return status;  
    }  
    public static Customer getcustloyeeById(int custID){  
    	Customer e=new Customer();  
          
        try{  
            Connection con=Customerdao.getConnection();  
            PreparedStatement ps=con.prepareStatement("Select * From Customers where custID=?");  
            ps.setInt(1,custID);  
            ResultSet rs=ps.executeQuery();  
            if(rs.next()){  
                e.setCustID(rs.getInt(1));  
                e.setName(rs.getString(2));  
                e.setSurname(rs.getString(3));  
                e.setNumber1(rs.getString(4));
                e.setNumber2(rs.getString(5));
                e.setNumber3(rs.getString(6));
    			e.setStore(rs.getString(7));
            }  
            con.close();  
        }catch(Exception ex){ex.printStackTrace();}  
          
        return e;  
    }  
    public static List<Customer> getAllCustomer(){  
        List<Customer> list=new ArrayList<Customer>();  
        try{  
            Connection con=Customerdao.getConnection();  
            PreparedStatement ps=con.prepareStatement("SELECT e.custID , e.name ,e.surname, e.storeName, GROUP_CONCAT(number1,\",\",number2,\",\",number3) as numbers\n"
            		+ "From  Customers as e \n"
            		+ "INNER JOIN Phone as cp ON e.custID = cp.custID \n"
            		+ "GROUP BY e.custID ");          
            ResultSet rs=ps.executeQuery();  
            while(rs.next()){  
            	Customer e=new Customer();  
                e.setCustID(rs.getInt(1));  
                e.setName(rs.getString(2));  
                e.setSurname(rs.getString(3));  
                e.setStore(rs.getString(4)); 
                e.setNumber1(rs.getString(5));  
                list.add(e);  
            }  
            con.close();  
        }catch(Exception e){e.printStackTrace();}  
          
        return list; 
        }

}